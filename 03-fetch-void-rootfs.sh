#!/bin/bash

# import vars
. importvars.sh

# message to user
echo "This script fetches the Void Linux rootfs base-install archive and installs it into your ZFS/LUKS partition currently mounted on ${TARGET}. You may want to examine the different possible rootfs archives available at http://alpha.de.repo.voidlinux.org/live/current to choose your desired base, e.g. if you're installing on 32bit or ARM or want to use musl libc, etc."

read -p "Which Void mirror to use [http://alpha.de.repo.voidlinux.org/live/current]: " VOIDMIRROR
VOIDMIRROR=${VOIDMIRROR:-http://alpha.de.repo.voidlinux.org/live/current}

read -p "Which rootfs to use? (You may well need to check this, as a newer rootfs than the displayed default may be available.) [void-x86_64-ROOTFS-20191109.tar.xz]: " VOIDROOTFS
VOIDROOTFS=${VOIDROOTFS:-void-x86_64-ROOTFS-20191109.tar.xz}

wget -N ${VOIDMIRROR}/${VOIDROOTFS}
wget -N ${VOIDMIRROR}/sha256.txt
wget -N ${VOIDMIRROR}/sha256.sig

echo "The checksum of ${VOIDROOTFS} is: "

SHASUM=`sha256sum ./${VOIDROOTFS}`

echo ${SHASUM}

if [[ `grep ${SHASUM} ./sha256.txt` ]]
then
    read -p "Checksum found. Hit <return> to proceeding." goodcheck
    tar xfv ./${VOIDROOTFS} -C ${TARGET}
    echo "You have successfully installed the Void Linux base system from ${VOIDROOTFS}. You may proceed to the next step; execute the following in the terminal:"
    echo ". 04-create-luks-keyfile.sh"
else
    echo "Checksum not found."
    read -p "Do you still want to proceed? (You probably don't.). If so, please type 'yes' in ALLCAPS: " badcheck
    if [ ${badcheck} == "YES" ]
    then
	tar xfv ./${VOIDROOTFS} -C ${TARGET}
	echo "You have installed the Void Linux base system from ${VOIDROOTFS}, though there was apparently a checksum mismatch. You may proceed with caution to the next step; execute the following in the terminal:"
	echo ". 04-create-luks-keyfile.sh"
    else
        echo "Cancelled."
    fi
fi

